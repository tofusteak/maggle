<?php

namespace Tofusteak\AngularApiBundle\Action;

use App\AppEvent;
use App\AppEvents;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;
use Tofusteak\AngularApiBundle\Entity\File;
use Tofusteak\AngularApiBundle\MaggleEvent;
use Tofusteak\AngularApiBundle\MaggleEvents;
use Tofusteak\AngularApiBundle\Serialization\GroupsExtractor;

class FileUpload
{
    private $eventDispatcher;
    private $serializer;
    private $groupsExtractor;
    private $container;
    private $managerRegistry;

    public function __construct(EventDispatcherInterface $eventDispatcher, SerializerInterface $serializer, GroupsExtractor $groupsExtractor, ContainerInterface $container, ManagerRegistry $managerRegistry)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->serializer = $serializer;
        $this->groupsExtractor = $groupsExtractor;
        $this->container = $container;
        $this->managerRegistry = $managerRegistry;
    }

    /**
     *     defaults={"_api_resource_class"=File::class, "_api_item_operation_name"="reset_password"},
     * @Route(
     *     name="file_upload",
     *     path="/{class}/upload",
     *     methods={"POST"}
     * )
     */
    public function __invoke($class, Request $request) // API Platform retrieves the PHP entity using the data provider then (for POST and
        // PUT method) deserializes user data in it. Then passes it to the action. Here $data
        // is an instance of Book having the given ID. By convention, the action's parameter
        // must be called $data.
    {
//        dump($this->serializer);

        $snakeNormalizer = new CamelCaseToSnakeCaseNameConverter();
        $classPath = 'App\Entity\\' . ucfirst($snakeNormalizer->denormalize(str_replace('-', '_', $class)));
//        dump($classPath);

        // Get class
        /** @var File $file */
        $file = new $classPath();

//        return new Response(null, 204);
        // Check voters
        // Deserialize
        // Persist
//        dump(realpath('./'));

        /** @var UploadedFile $uploadedFile */
        $filesIndexes = $request->files->keys();
        $uploadedFile = $request->files->get($filesIndexes[0]);

        $file->filename = $uploadedFile->getClientOriginalName();

        $dest = date('U');
        $path = $this->container->getParameter('tofusteak_angular_api.tmp_upload_dir') . '/' . $dest;
        $uploadedFile->move($path, $file->filename);

        $file->filepath = $dest . '/' . rawurlencode($file->filename);

        $em = $this->managerRegistry->getManager();
        $em->persist($file);
        $em->flush();

//        $this->eventDispatcher->dispatch(MaggleEvents::POST_FILE_UPLOAD, new MaggleEvent($file));

        $this->groupsExtractor->getSerializationGroups($class, 'POST', $classPath);

        return new Response(
            $this->serializer->serialize($file, 'jsonld', ['groups' => ['file.anonymous.show']]),
            200
        );
    }
}
