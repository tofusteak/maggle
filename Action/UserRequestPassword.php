<?php

namespace Tofusteak\AngularApiBundle\Action;

use App\AppEvent;
use App\AppEvents;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Tofusteak\AngularApiBundle\MaggleEvent;
use Tofusteak\AngularApiBundle\MaggleEvents;

class UserRequestPassword
{
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route(
     *     name="user_request_password",
     *     path="/users/{id}/request_password",
     *     defaults={"_api_resource_class"=User::class, "_api_item_operation_name"="request_password"},
     *     methods={"GET"}
     * )
     */
    public function __invoke($data) // API Platform retrieves the PHP entity using the data provider then (for POST and
        // PUT method) deserializes user data in it. Then passes it to the action. Here $data
        // is an instance of Book having the given ID. By convention, the action's parameter
        // must be called $data.
    {
        $this->eventDispatcher->dispatch(MaggleEvents::PASSWORD_REQUESTED, new MaggleEvent($data));

        return new Response(null, 204);
    }
}