<?php

namespace Tofusteak\AngularApiBundle\Serialization;

use Symfony\Component\Filesystem\Filesystem;

class SerializationConverter
{
    /**
     * @var string
     */
    protected $bundlePath;

    /**
     * @var SerializationMetadataReader
     */
    protected $serializationMetadataReader;

    /**
     * SerializationConverter constructor.
     * @param string $bundlePath
     * @param SerializationMetadataReader $serializationMetadataReader
     */
    public function __construct($bundlePath, SerializationMetadataReader $serializationMetadataReader)
    {
        $this->bundlePath = $bundlePath;
        $this->serializationMetadataReader = $serializationMetadataReader;
    }


    public function convert()
    {
        $fileSystem = new Filesystem;
        $yamlPath = $this->bundlePath.'/../config/serializer/';

        $metadata = $this->serializationMetadataReader->getMetadata();
        foreach ($metadata as $className => $classMetadata) {
            /** @var ClassMetadata $classMetadata */
            if (count($classMetadata->getAttributesMetadata()) > 0) {
                $yaml = $classMetadata->getName().": \n  attributes:\n";

                $isFirst = true;
                foreach ($classMetadata->getAttributesMetadata() as $k => $attributeMetadata) {
                    $yaml .= ($isFirst ? "" : "\n")."    {$attributeMetadata->getName()}:\n      groups:\n";
                    $isFirst = false;
                    foreach ($attributeMetadata->getSerializationGroups() as $group) {
                        $yaml .= "        - $group\n";
                    }
                }

                $fileSystem->dumpFile($yamlPath.'/'.$classMetadata->getCaretCase().'.yml', $yaml);
            }
        }
    }
}
