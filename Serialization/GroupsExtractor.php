<?php

namespace Tofusteak\AngularApiBundle\Serialization;

use App\Security\ConditionalFields;
use Tofusteak\AngularApiBundle\Security\User\RolesExtractor;

class GroupsExtractor
{
    protected $rolesExtractor;

    public function __construct(RolesExtractor $rolesExtractor)
    {
        $this->rolesExtractor = $rolesExtractor;
    }

    /**
     * Get all role labels for connected user
     *
     * @param string $resourceName The resource name
     * @param string $action The reached action
     * @return array
     */
    public function getSerializationGroups($resourceName, $action, $resourceClass) : array
    {
        $serializationGroupModel = $resourceName . '.%s.' . $action;


        if (class_exists('App\Security\ConditionalFields')) {
            $groups = [sprintf($serializationGroupModel, 'anonymous')];

            // @todo to be replaced by a security layer much separated with the serialization because it's not the serialization that should determinate the security
            // @todo check if ConditionalFields class exists and use fully qualified name space everywhere
            foreach ($this->rolesExtractor->getRoleLabels() as $role) {
                $group = sprintf($serializationGroupModel, $role);
                $groups[] = $group;

                if (isset(ConditionalFields::getSecuredFields()[$group])) {
                    $fields = ConditionalFields::getSecuredFields()[$group];
                    foreach ($fields as $k => $field) {
                        if ($this->authChecker->isGranted([$field], new $resourceClass)) { // @todo this should not be a new object (ref new $attributes['resource_class']) this should be the real object
                            $context['groups'][] = sprintf('%s.%s.%s__SECURED_%s', $resourceName, $role, $action, $k);
                        }
                    }
                }
            }

            return $groups;
        }

        // @todo when conditional field be rewriten just remove the foreach and $groups initialisation and uncomment the following code
        return array_map(function ($roleLabel) use ($serializationGroupModel) {
            return sprintf($serializationGroupModel, $roleLabel);
        }, $this->rolesExtractor->getRoleLabels());
    }
}
