<?php

namespace Tofusteak\AngularApiBundle\Serialization;

use Symfony\Component\Serializer\Mapping\AttributeMetadata as BaseAttributeMetadata;

class AttributeMetadata extends BaseAttributeMetadata
{
    public $type;
    public $doctrineType;
    public $isCollection = false;
    public $isEnum = false;
    public $isNullable = false;
    public $keysMatter = false;
    private $options = [];

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDoctrineType()
    {
        return $this->doctrineType;
    }

    /**
     * @param mixed $doctrineType
     */
    public function setDoctrineType($doctrineType)
    {
        $this->doctrineType = $doctrineType;
    }

    /**
     * @return mixed
     */
    public function getIsCollection()
    {
        return $this->isCollection;
    }

    /**
     * @param mixed $isCollection
     */
    public function setIsCollection($isCollection)
    {
        $this->isCollection = $isCollection;
    }

    /**
     * @return mixed
     */
    public function getIsEnum()
    {
        return $this->isEnum;
    }

    /**
     * @param mixed $isEnum
     */
    public function setIsEnum($isEnum)
    {
        $this->isEnum = $isEnum;
    }

    /**
     * @return mixed
     */
    public function getIsNullable()
    {
        return $this->isNullable;
    }

    /**
     * @param mixed $isNullable
     */
    public function setIsNullable($isNullable)
    {
        $this->isNullable = $isNullable;
    }

    /**
     * @return mixed
     */
    public function getKeysMatter()
    {
        return $this->keysMatter;
    }

    /**
     * @param mixed $keysMatter
     */
    public function setKeysMatter($keysMatter)
    {
        $this->keysMatter = $keysMatter;
    }

    public function toConstructor()
    {
        return sprintf('\'%s\', %s, %s, %s', $this->getType(), $this->getIsCollection(), $this->getIsEnum(), $this->getIsNullable(), $this->getKeysMatter());
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        $arrayMetadata = array_filter([
            'type' => $this->getDoctrineType(),
            'isCollection' => $this->isCollection,
            'isEnum' => $this->isEnum,
            'isNullable' => $this->isNullable,
            'keysMatter' => $this->keysMatter
        ]);

        return array_merge($arrayMetadata, $this->options);
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @var string $key
     * @var mixed  $value
     */
    public function addOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * @var string $key
     * @return mixed
     */
    public function getOption($key)
    {
        return $this->options[$key];
    }

    /**
     * @return boolean
     */
    public function hasOption($option)
    {
        return isset($this->options[$key]);
    }

    public function getSerializationGroups() {
        return array_unique(array_map(function($group) {
            list($entity, $role, $action) = explode('.', $group);
            return sprintf('%s.%s.%s', $entity, $role, $action);
        }, $this->getGroups()));
    }
}
