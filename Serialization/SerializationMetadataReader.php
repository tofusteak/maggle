<?php

namespace Tofusteak\AngularApiBundle\Serialization;

use ApiPlatform\Core\Bridge\Symfony\Routing\Router;
use App\Entity\Demand;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Tofusteak\AngularApiBundle\Annotation\Enum;
use Tofusteak\AngularApiBundle\Annotation\ExtraMetadata;
use Tofusteak\AngularApiBundle\Annotation\Type;

class SerializationMetadataReader
{
    /**
     * @var string
     */
    public $bundlePath;

    /**
     * @var array
     */
    public $metadata = [];

    /**
     * @var enum
     */
    public $enumMetadata = [];

    /**
     * @var array
     */
    public $groups = [];

    /**
     * @var array
     */
    public $groupsByClass = [];

    /**
     * @var CachedReader
     */
    public $annotationReader;

    /**
     * @var Router
     */
    public $router;

    public $securedFields = [];

    /**
     * MlSerializationMetadataReader constructor.
     * @param string $bundlePath
     * @param CachedReader $annotationReader
     * @param Router $router
     */
    public function __construct($bundlePath, CachedReader $annotationReader, Router $router)
    {
        $this->bundlePath = $bundlePath;
        $this->annotationReader = $annotationReader;
        $this->router = $router;
    }

    public function getMetadata()
    {
        if (!count($this->metadata)) {
            // Get route associations (Activity => activities)
            $routes = $this->router->getRouteCollection();
            $routeAssociations = [];
            foreach ($routes as $route) {
                $routeAssociations[$route->getDefault('_api_resource_class')] = explode('/', $route->getPath())[1];
            }

            $yamlPath = $this->bundlePath.'/../config/ml-serialization/';
            $finder = new Finder();
            $finder->files()->in($yamlPath);

            foreach ($finder as $file) {
                try {
                    $parsedYaml = Yaml::parse($file->getContents());
                } catch (ParseException $e) {
                    $e->setParsedFile($file->getRelativePathname());

                    throw $e;
                }

                // First level is className
                foreach ($parsedYaml as $className => $groups) {
                    $class = $this->getClass($className);

                    $parentClass = get_parent_class($className);
                    if ($parentClass) {
                        $parentClass = $this->getClass(get_parent_class($className));
                    }

                    if (isset($routeAssociations[$className])) {
                        $class->setPath($routeAssociations[$className]);
//                        if ($parentClass) {
//                            $class->setPath($routeAssociations[$parentClass->getName()]);
//                        }
                    }

                    foreach ($groups as $group => $attributes) {
                        $groups[lcfirst($class->getShortName()).'.'.$group] = [];
                        $this->getAttributesDescription($className, lcfirst($class->getShortName()).'.'.$group, $attributes);

                        if ($parentClass) {
                            $groups[lcfirst($class->getShortName()).'.'.$group] = [];
                            $this->getAttributesDescription($parentClass->getName(), lcfirst($class->getShortName()).'.'.$group, $attributes);
                        }
                    }
                }
            }
        }

        return $this->metadata;
    }

    public function getAttributesDescription($className, $group, $attributes)
    {
        $classMetadata = $this->getClass($className);

        foreach ($attributes as $attributeDescription) {
            $subValues = null;

            $attributePath = '';
            switch (true) {
                case is_string($attributeDescription):
                    $attributePath = $attributeDescription;
                    break;
                case is_string($attributeDescription) && strpos($attributeDescription, '.') > -1:
                    $attributePath = $attributeDescription;
                    break;
                case count($attributeDescription) > 1 && is_numeric($attributeDescription[1]):
                    $attributePath = $attributeDescription[0];
                    $maxDepth = $attributeDescription[1];
                    break;
                case is_array($attributeDescription):
                    $attributePath = array_keys($attributeDescription)[0];
                    $subValues = $attributeDescription[$attributePath];
                    break;
            }

            $path = null;
            if (false !== strpos($attributePath, '.')) {
                $path = explode('.', $attributePath);
                $attributeName = array_shift($path);
            } else {
                $attributeName = $attributePath;
            }

            $isSecured = false;
            if (false !== strpos($attributeName, '*')) {
                $attributeName = str_replace('*', '', $attributeName);
                $isSecured = true;
                $this->securedFields[$group.'.'.$attributeName] = null;
            }

            /** @var ClassMetadata $classMetadata */
            $attributeMetadata = $this->getAttributeMetadata($classMetadata->getCaretCase(), $attributeName);

            if ($attributeName === 'locale' && strpos($classMetadata->getName(), 'Translation')) {
                $attributeMetadata->addOption('hidden', true);
            }

            // Get annotations from attributes if any, else get them from method
            $annotations = [];
            if (property_exists($className, $attributeName)) {
                $annotations = $this->annotationReader->getPropertyAnnotations(new \ReflectionProperty($className, $attributeName));
            } elseif (method_exists($className, 'get'.ucfirst($attributeName))) {
                $annotations = $this->annotationReader->getMethodAnnotations(new \ReflectionMethod($className, 'get'.ucfirst($attributeName)));
            }

            foreach ($annotations as $annotation) {
                try {
                    switch (true) {
                        case $annotation instanceof ExtraMetadata:
                            $attributeMetadata->setOptions($annotation->value);
                            break;
                        case $annotation instanceof Enum:
                            $attributeMetadata->setType('number');
                            $attributeMetadata->setDoctrineType('number');
                            $enum = $this->getEnum($annotation->value);
                            $attributeMetadata->addOption('enumType', $enum->getCaretCase());
                            break;
                        case $annotation instanceof Type:
                            $targetClassMetadata = $this->getClass($annotation->value);
                            $attributeMetadata->setType($targetClassMetadata->getCaretCase());
                            $attributeMetadata->setDoctrineType($targetClassMetadata->getCaretCase());

                            $this->processNext($annotation->value, $group, $path, $subValues);
                            break;
                        case $annotation instanceof Column:
                            if (!$attributeMetadata->getType()) {
                                $angularCorrespondance = [
                                    'array' => 'any[]',
                                    'json_array' => 'any',
                                    'smallint' => 'number',
                                    'float' => 'number',
                                    'phone_number' => 'string',
                                    'text' => 'string',
                                    'date' => 'string',
                                    'datetime' => 'string'
                                ];

                                if (isset($angularCorrespondance[$annotation->type])) {
                                    $attributeMetadata->setType($angularCorrespondance[$annotation->type]);
                                } else {
                                    $attributeMetadata->setType($annotation->type);
                                }

                                $attributeMetadata->setDoctrineType($annotation->type);
                            }
                            break;
                        case $annotation instanceof OneToMany:
                        case $annotation instanceof ManyToMany:
                            $attributeMetadata->setIsCollection(true);
                        case $annotation instanceof OneToOne:
                        case $annotation instanceof ManyToOne:
                            if (false === strpos($annotation->targetEntity, '\\')) {
                                $classPath = explode('\\', $classMetadata->getName());
                                array_pop($classPath);
                                $targetEntity = implode('\\', $classPath).'\\'.$annotation->targetEntity;
                                $targetClassMetadata = $this->getClass($targetEntity);
                                $attributeMetadata->setType($targetClassMetadata->getCaretCase());
                                $attributeMetadata->setDoctrineType($targetClassMetadata->getCaretCase());
                            } else {
                                $targetEntity = $annotation->targetEntity;
                                $targetClassMetadata = $this->getClass($targetEntity);
                                $attributeMetadata->setType($targetClassMetadata->getCaretCase());
                                $attributeMetadata->setDoctrineType($targetClassMetadata->getCaretCase());
                            }

                            $this->processNext($targetEntity, $group, $path, $subValues);
                    }
                } catch (\Exception $e) {
                    dump($annotation);
                    throw new \Exception("Unable to process {$className}->{$attributeName} : \"{$e->getMessage()}");
                }
            }

            if ($attributeName === 'translations' && method_exists($className, 'getTranslationEntityClass')) {
                $translationClass = $className::getTranslationEntityClass();
                $targetClassMetadata = $this->getClass($translationClass);
                $attributeMetadata->setType($targetClassMetadata->getCaretCase());
                $attributeMetadata->setDoctrineType($targetClassMetadata->getCaretCase());
                $attributeMetadata->setIsCollection(true);
                $attributeMetadata->setKeysMatter(true);
                $this->processNext($translationClass, $group, $path, $subValues);
            }

            $this->addGroup($group.($isSecured ? '__SECURED_'.strtoupper(str_replace(['-', '.'], '_', $group).'_'.$attributeName) : ''), $classMetadata->getCaretCase(), $attributeMetadata);

            if (isset($maxDepth)) {
                $attributeMetadata->setMaxDepth($maxDepth);
            }
        }
    }

    protected function processNext($className, $group, $path, $subValues) {
        switch (true) {
            case $path && $subValues :
                $this->getAttributesDescription($className, $group, [[implode('.', $path) => $subValues]]);
                break;
            case $path :
                $this->getAttributesDescription($className, $group, [implode('.', $path)]);
                break;
            case $subValues :
                $this->getAttributesDescription($className, $group, $subValues);
                break;
        }
    }

    protected function getClass($className) {
        $className = ltrim($className, '\\');
        $class = new ClassMetadata($className);

        if (!isset($this->metadata[$class->getCaretCase()])) {
            $this->metadata[$class->getCaretCase()] = $class;

            return $class;
        }

        return $this->metadata[$class->getCaretCase()];
    }

    protected function getEnum($enumClassName) {
        $enumClassName= ltrim($enumClassName, '\\');
        $enum = new EnumMetadata($enumClassName);

        if (!isset($this->enumMetadata[$enum->getCaretCase()])) {
            $this->enumMetadata[$enum->getCaretCase()] = $enum;

            return $enum;
        }

        return $this->enumMetadata[$enum->getCaretCase()];
    }

    protected function getAttributeMetadata($className, $attributeName) {
        $className = ltrim($className, '\\');

        /** @var ClassMetadata $classMetadata */
        $classMetadata = $this->metadata[$className];
        if (isset($classMetadata->attributesMetadata[$attributeName])) {
            return $classMetadata->attributesMetadata[$attributeName];
        }

        $attributeMetadata = new AttributeMetadata($attributeName);
        $classMetadata->addAttributeMetadata($attributeMetadata);

        return $attributeMetadata;
    }

    protected function addGroup($group, $className, AttributeMetadata $attributeMetadata) {
        // Add to $groups
        if (!isset($this->groups[$group])) {
            $this->groups[$group] = [$className => [$attributeMetadata->getName()]];
        } elseif (!isset($this->groups[$group][$className])) {
            $this->groups[$group][$className] = [$attributeMetadata->getName()];
        } elseif (!in_array($attributeMetadata->getName(), $this->groups[$group][$className])) {
            $this->groups[$group][$className][] = $attributeMetadata->getName();
        }

        // Add to $groupsByClass
        if (!isset($this->groupsByClass[$className])) {
            $this->groupsByClass[$className] = [$group => [$attributeMetadata->getName()]];
        } elseif (!isset($this->groupsByClass[$className][$group])) {
            $this->groupsByClass[$className][$group] = [$attributeMetadata->getName()];
        } elseif (!in_array($attributeMetadata->getName(), $this->groupsByClass[$className][$group])) {
            $this->groupsByClass[$className][$group][] = $attributeMetadata->getName();
        }

        $attributeMetadata->addGroup($group);
    }
}
