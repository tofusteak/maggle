<?php

namespace Tofusteak\AngularApiBundle\Serialization;

use Symfony\Component\Serializer\Mapping\ClassMetadata as BaseClassMetadata;

class ClassMetadata extends BaseClassMetadata {

    /**
     * @var string Path for this class (Activity => activities)
     */
    private $path;

    /**
     * Returns class name from full namespace
     *
     * @return string
     */
    public function getShortName()
    {

        return preg_replace('/.*\\\\(.*)/', '$1', $this->name);
    }

    /**
     * Returns class name as caret case
     *
     * @return string
     */
    public function getCaretCase()
    {
        $snakeCasedName = '';
        $className = lcfirst($this->getShortName());

        $len = strlen($className);
        for ($i = 0; $i < $len; ++$i) {
            if (ctype_upper($className[$i])) {
                $snakeCasedName .= '-'.strtolower($className[$i]);
            } else {
                $snakeCasedName .= strtolower($className[$i]);
            }
        }

        return $snakeCasedName;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}
