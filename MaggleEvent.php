<?php

namespace Tofusteak\AngularApiBundle;

use Dunglas\ApiBundle\Api\ResourceInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * MaggleEvent
 */
class MaggleEvent extends Event
{
    /**
     * @var object|array
     */
    private $data;

    /**
     * @param object|array      $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Gets related data.
     *
     * @return object|array
     */
    public function getData()
    {
        return $this->data;
    }
}
