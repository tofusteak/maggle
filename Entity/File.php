<?php

namespace Tofusteak\AngularApiBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * The most generic type of item.
 */
class File
{
    /**
     * @var string
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=127)
     * @Assert\Length(max=127)
     *
     * @Assert\Type(type="string")
     */
    public $filepath;

    /**
     * @var string
     *
     * @ORM\Column
     * @Assert\Type(type="string")
     * @Assert\NotNull()
     */
    public $filename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $createdAt;

    /**
     * File constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return '/files/'.$this->getId();
    }

}
