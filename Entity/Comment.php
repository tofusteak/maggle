<?php

namespace Tofusteak\AngularApiBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Tofusteak\AngularApiBundle\Annotation\ExtraMetadata;

/**
 * The most generic type of item.
 */
class Comment
{
    /**
     * @var string
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @ExtraMetadata({"type": "textarea"})
     */
    public $content;

    /**
     * To store the content if the message has been censored
     *
     * @ORM\Column(type="text", nullable=true)
     */
    public $originalContent;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     * @Assert\NotNull()
     */
    public $publishedAt;

    /**
     * @TODO Set targetEntity in external mapping file
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    public $reportedBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     * @var User
     */
    public $author;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comment", inversedBy="replies")
     * @Assert\Expression(
     *     "!this.replyTo or (this.replyTo and !this.replyTo.replyTo)",
     *     message="Vous ne pouvez pas répondre à un message qui est déjà une réponse."
     * )
     * @ExtraMetadata({"type": "string"})
     */
    public $replyTo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="replyTo")
     */
    public $replies;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable = true)
     */
    public $repliesCount;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

}
