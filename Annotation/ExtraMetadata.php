<?php

/*
 * This file is part of the DunglasApiBundle package.
 *
 * (c) Kévin Dunglas <dunglas@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tofusteak\AngularApiBundle\Annotation;

/**
 * ExtraMetadata annotation.
 *
 * @author Jérémy Hubert <jeremy@tofusteak.fr>
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD"})
 */
class ExtraMetadata
{
    public $value;

    /**
     * @return string
     */
    function __toString()
    {
        return $this->value;
    }
}
