<?php

namespace App\EventSubscriber;

use Doctrine\Common\Cache\CacheProvider;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;

/**
 * Store last entity change set in memory, so that it could be
 * usable in postUpdate event.
 */
class EntityChangeSetStorageSubscriber implements EventSubscriber
{
    /**
     * @var CacheProvider
     */
    private $cache;

    /**
     * @param CacheProvider $cacheStorage
     */
    public function __construct(CacheProvider $cacheStorage)
    {
        $this->cache = $cacheStorage;
    }

    /**
     * Store last entity change set in memory.
     *
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $entity = $event->getEntity();
        $this->cache->setNamespace(get_class($entity));
        $this->cache->save($entity->getId(), $event->getEntityChangeSet());
    }

    /**
     * Release the memory.
     */
    public function onClear()
    {
        $this->clearCache();
    }

    /**
     * Clear cache.
     */
    private function clearCache()
    {
        $this->cache->flushAll();
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::preUpdate,
            Events::onClear,
        ];
    }
}