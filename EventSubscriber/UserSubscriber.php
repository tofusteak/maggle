<?php

namespace Tofusteak\AngularApiBundle\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use AppBundle\Manager\Mailer;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Tofusteak\AngularApiBundle\MaggleEvent;
use Tofusteak\AngularApiBundle\MaggleEvents;

final class UserSubscriber implements EventSubscriberInterface
{
    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * UserSubscriber constructor.
     * @param EncoderFactoryInterface $encoderFactory
     * @param TokenStorageInterface   $tokenStorage
     * @param ObjectManager $objectManager
     * @param Mailer $mailer
     */
    public function __construct(EncoderFactoryInterface $encoderFactory, TokenStorageInterface $tokenStorage, ObjectManager $objectManager)
    {
        $this->encoderFactory = $encoderFactory;
        $this->tokenStorage = $tokenStorage;
        $this->objectManager = $objectManager;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['generatePassword', 40],
            ],
            MaggleEvents::PASSWORD_REQUESTED => 'onPasswordRequest',
            MaggleEvents::PASSWORD_RESET => 'onPasswordReset',
        ];
    }

    public function generatePassword(GetResponseForControllerResultEvent $event)
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$user instanceof User || Request::METHOD_POST !== $method) {
            return;
        }

        $password = substr(md5(rand()), 0, 12);
        $user->setPassword($this->encoderFactory->getEncoder($user)->encodePassword($password, $user->getSalt()));

        // Send mail with password
    }

    /**
     * @param MaggleEvent $event
     */
    public function onPasswordRequest(MaggleEvent $event)
    {
        /** @var User $user */
        $user = $event->getData();

        // First, we create a password request token
        $user->resetPasswordToken = md5(uniqid(null, true));
        $user->resetPasswordRequestedAt = new \DateTime();

//        $this->mailer->send(
//            'user_request_password',
//            $user->getEmail(),
//            [
//                'firstName' => $user->getFirstName(),
//                'token'     => $user->getResetPasswordToken()
//            ]
//        );

        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }

    /**
     * @param MaggleEvent $event
     */
    public function onPasswordReset(MaggleEvent $event)
    {
        /** @var User $user */
        $user = $event->getData();
        $user->password = $this->encoderFactory->getEncoder($user)->encodePassword($user->plainPassword, $user->salt);

        $this->objectManager->persist($user);
        $this->objectManager->flush();
    }
}
