<?php

namespace Tofusteak\AngularApiBundle\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use AppBundle\Manager\Mailer;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Tofusteak\AngularApiBundle\Entity\File;
use Tofusteak\AngularApiBundle\MaggleEvent;
use Tofusteak\AngularApiBundle\MaggleEvents;

final class FileUploadSubscriber implements EventSubscriberInterface
{

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * UserSubscriber constructor.
     * @param TokenStorageInterface   $tokenStorage
     * @param ObjectManager $objectManager
     * @param Mailer $mailer
     */
    public function __construct(TokenStorageInterface $tokenStorage, ObjectManager $objectManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->objectManager = $objectManager;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['uploadFile', EventPriorities::PRE_WRITE],
        ];
    }

    public function uploadFile(GetResponseForControllerResultEvent $event)
    {
        $file = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$file instanceof File || Request::METHOD_POST !== $method) {
            return;
        }
    }

}
