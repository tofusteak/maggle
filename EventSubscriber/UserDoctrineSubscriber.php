<?php

// src/App/EventSubscriber/BookMailSubscriber.php

namespace Tofusteak\AngularApiBundle\EventSubscriber;

use App\Entity\User;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

final class UserDoctrineSubscriber implements EventSubscriber
{
    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * UserSubscriber constructor.
     * @param EncoderFactoryInterface $encoderFactory
     * @param TokenStorage            $tokenStorage
     */
    public function __construct(EncoderFactoryInterface $encoderFactory, TokenStorage $tokenStorage)
    {
        $this->encoderFactory = $encoderFactory;
        $this->tokenStorage = $tokenStorage;
    }


    public function getSubscribedEvents()
    {
        return [ Events::preUpdate ];
    }

    public function preUpdate(PreUpdateEventArgs $event) {
        $user = $event->getEntity();

        $connectedUser = $this->tokenStorage->getToken()->getUser();
        if (!$user instanceof User) {
            return;
        }

        if (is_string($connectedUser) && $user->getResetPasswordToken()) {
            $user->setPassword($this->encoderFactory->getEncoder($user)->encodePassword($user->plainPassword, $user->getSalt()));
            return;
        }

        if ($user->getId() === $connectedUser->getId() &&
            $user->plainPassword !== null
        ) {
            $user->setPassword($this->encoderFactory->getEncoder($user)->encodePassword($user->plainPassword, $user->getSalt()));
        }

        if ($connectedUser->hasRole('ROLE_ADMIN') && $user->plainPassword !== null) {
            $user->setPassword($this->encoderFactory->getEncoder($user)->encodePassword($user->plainPassword, $user->getSalt()));
        }
    }
}
