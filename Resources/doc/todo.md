- Generate enums (& enum annotation)

- Generate models (with .toJson(group), handling enums)
  - ? Use @Iri to comment properties

- Generate front routes for FrontRouter sf2 service

- Generate Components :
  - List :
    - Handle filters and sorting (do a DemandFilter with a .toGetRequest ?)
    - Handle pagination (do a paginationComponent that subscribes to cget)

  - Show
    - Show deep relations
    - ? Use @Iri when available

  - Edit / Create
    - Manage deep relations
    - Manage validation

  - Make a resource root component (demandComponent) containing routing

- Services
  - Keep in memory for last N elements (N determined for each service)

- Generate Admin Components with same method (only change directory and .toJson groups)

- Make app.ts
