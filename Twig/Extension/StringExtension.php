<?php

namespace Tofusteak\AngularApiBundle\Twig\Extension;

use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class StringExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('lcfirst', 'lcfirst'),
            new \Twig_SimpleFilter('ucfirst', 'ucfirst'),
            new \Twig_SimpleFilter('string', array($this, 'string')),
            new \Twig_SimpleFilter('snaked', array($this, 'snaked')),
            new \Twig_SimpleFilter('camelized', array($this, 'camelized'))
        ];
    }

    public function string($value)
    {
        if (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        }

        return (string) $value;
    }

    public function snaked($value, $character = null)
    {
        $snakeNormalizer = new CamelCaseToSnakeCaseNameConverter();
        $value = lcfirst($value);

        if ($character) {
            return str_replace('_', $character, $snakeNormalizer->normalize($value));
        }

        return $snakeNormalizer->normalize($value);
    }

    public function camelized($value)
    {
        $snakeNormalizer = new CamelCaseToSnakeCaseNameConverter();

        return $snakeNormalizer->denormalize(strtolower($value));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'string';
    }
}
