<?php

namespace Tofusteak\AngularApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * A console command to export routes to angular2 classes
 */
class ExportServiceCommand extends ContainerAwareCommand
{
    /**
     * Destination path for angular2 ts files
     *
     * @var string path
     */
    private $path;

    /**
     * @var CamelCaseToSnakeCaseNameConverter
     */
    private $snakeConverter;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return parent::isEnabled();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('angular:export:services')
            ->setDefinition([
                new InputOption('method', 'm', InputOption::VALUE_NONE, 'Export only a single method from a class'),
                new InputOption('class', 'c', InputOption::VALUE_NONE, 'Export only a single class')
            ])
            ->setDescription('Exports API routes to angular classes')
            ->setHelp(<<<'EOF'
The <info>%command.name%</info> allows to export API routes to angular classes:

  <info>php %command.full_name%</info>

Add <info>--method</info> if you want to render in console a single method from one of detected classes:

  <info>php %command.full_name% --method</info>
  or
  <info>php %command.full_name% -m</info>

Add <info>--class</info> if you want to export only one class:

  <info>php %command.full_name% --class</info>
  or
  <info>php %command.full_name% -c</info>

EOF
            )
        ;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException When route does not exist
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io         = new SymfonyStyle($input, $output);
        $routes     = $this->getContainer()->get('router')->getRouteCollection();
        $this->path = $this->getContainer()->getParameter('tofusteak_angular_api.destination_path').'/shared/services/';
        $metadataFactory = $this->getContainer()->get('api_platform.metadata.resource.metadata_factory');
        $this->snakeConverter = new CamelCaseToSnakeCaseNameConverter();
        $this->templating = $this->getContainer()->get('templating');

        $classes = [];
        foreach ($routes as $route) {

            /* @var Route $route */
            $entityClass = $route->getDefault('_api_resource_class');
            $resource    = null;
            if ($entityClass) {
                $resource = $metadataFactory->create($entityClass)->getShortName();
            }

            if ($resource) {
                /** @var Route $route */
                if (!isset($classes[$resource])) {
                    $classes[$resource] = [
                        'name' => $resource,
                        'methods' => []
                    ];
                }

                // Remove _format parameter from route
                $path = preg_replace('/\.?\{\_format\}/', '', $route->getPath());
                // Extract all route parameters to generate javascript routes
                preg_match_all('/\{(.*)\}/U', $path, $parameters);
                foreach($route->getMethods() as $method) {
//                    var_dump($route);
                    $action = $this->getContainer()->get('api_platform.action.delete_item');
//                    var_dump($action);
                    $name = preg_replace('/.*\:\:(.*)Action$/', '$1', $route->getDefault('_controller'));

                    // This is a default ApiPlatform action if $name === _controller
                    if ($name === $route->getDefault('_controller')) {
                        $name = $route->getDefault('_api_item_operation_name') ?? $route->getDefault('_api_collection_operation_name').'Collection';
                    }

                    // Remove .{_format} from path
                    $path = preg_replace('/\.?\{\_format\}/', '', $route->getPath());
                    $path = str_replace(' + \'\'', '', '\''.preg_replace('/\{(.*)\}/U', '\' + $1 + \'', $path).'\'');

                    if (in_array($method, ['POST', 'PUT'])) {
                        $parameters[1][] = strtolower($resource);
                    }

                    $idParameters = array_filter($parameters[1], function($v, $k) {
                        return strpos($v, 'id') === 0 || strpos($v, 'Id') > 0;
                    }, ARRAY_FILTER_USE_BOTH);

                    $classes[$resource]['methods'][str_pad($method, 5).$name] = [
                        'path' => $path,
                        'name' => $name,
                        'parameters' => $parameters[1],
                        'idParameters' => $idParameters,
                        'method' => $method
                    ];
                }
            }
        }

        if (!$input->getOption('method')) {
            $fs = new Filesystem;
            if (!$fs->exists($this->path)) {
                $fs->mkdir($this->path);
            }

            if ($input->getOption('class')) {
                $class = $io->choice('Please select the entity you want to export', array_keys($classes));
                $this->dumpClass($classes[$class], $fs);
            } else {
                foreach ($classes as $class) {
                    $this->dumpClass($class, $fs);
                }

                $keys = array_keys($classes);
                sort($keys);
                $fs->dumpFile(
                    $this->path.'index.ts',
                    $this->getContainer()->get('templating')->render('TofusteakAngularApiBundle:Service:index.ts.twig', ['resources' => $keys])
                );
            }
        } else {
            $class = $io->choice('Please select the entity related to the export you\'re asking for', array_keys($classes));
            $method = $io->choice('Please select the method you would like to export', array_keys($classes[$class]['methods']));
            $io->success('Here is your generated method:');
            $method = $this->getContainer()->get('templating')->render('TofusteakAngularApiBundle:Service:action.ts.twig', [
                'resource' => $classes[$class],
                'method' => $classes[$class]['methods'][$method]
            ]);

            // Remove one whitespace because SymfonyStyle block adds a space, which breaks copy pasted indentation
            $method = str_replace("\n ", "\n", "\n".$method);

            $io->block($method, null, 'fg=white;bg=blue');
        }
    }

    /**
     * Writes angular2 class representing a class to file
     *
     * @param array $class
     * @param Filesystem $fs
     *
     * @throws \Twig_Error
     */
    private function dumpClass(array $class, Filesystem $fs)
    {
        $templateFile = $this->templating->exists('TofusteakAngularApiBundle:'.$class['name'].':service.ts.twig')
            ? 'TofusteakAngularApiBundle:'.$class['name'].':service.ts.twig'
            : 'TofusteakAngularApiBundle:Service:service.ts.twig';

        $fs->dumpFile(
            $this->path.$this->getCaretCase($class['name']).'.ts',
            $this->templating->render($templateFile, ['resource' => $class])
        );
    }

    private function getCaretCase(string $className) {

        return str_replace('_', '-', $this->snakeConverter->normalize(lcfirst($className)));
    }
}
