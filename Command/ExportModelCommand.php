<?php

namespace Tofusteak\AngularApiBundle\Command;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Tofusteak\AngularApiBundle\Annotation\Enum;
use Tofusteak\AngularApiBundle\Annotation\Model;
use Tofusteak\AngularApiBundle\Serialization\ClassMetadata;
use Tofusteak\AngularApiBundle\Serialization\EnumMetadata;
use Tofusteak\AngularApiBundle\Serialization\SerializationConverter;
use Tofusteak\AngularApiBundle\Serialization\SerializationMetadataReader;

/**
 * A console command to export routes to angular2 classes
 */
class ExportModelCommand extends ContainerAwareCommand
{
    /**
     * @var array
     */
    protected $yamlSerializationGroups = [];

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var array
     */
    private $enumClasses = [];

    /**
     * @var array
     */
    private $modelClasses = [];

    /**
     * @var array
     */
    private $groups = [];

    /**
     * @var array
     */
    private $propertiesByGroup = [];

    /**
     * @var CamelCaseToSnakeCaseNameConverter
     */
    private $snakeConverter;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var CachedReader
     */
    private $annotationReader;

    /**
     * @var array
     */
    private $entitiesNameList = [];

    /**
     * @var SerializationMetadataReader
     */
    private $reader;

    /**
     * @var SerializationConverter
     */
    private $converter;

    /**
     * ExportModelCommand constructor.
     * @param Reader $annotationReader
     * @param SerializationConverter $converter
     * @param SerializationMetadataReader $reader
     * param TwigEngine $twigEngine
     */
    public function __construct(Reader $annotationReader, SerializationConverter $converter, SerializationMetadataReader $reader)
    {
        parent::__construct();
        $this->annotationReader = $annotationReader;
        $this->converter = $converter;
        $this->reader = $reader;
//        $this->templating = $twigEngine;
    }


    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return parent::isEnabled();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('angular:export:model')
            ->setDefinition([
                new InputOption('method', 'm', InputOption::VALUE_NONE, 'Export only a single method from a class'),
                new InputOption('class', 'c', InputOption::VALUE_NONE, 'Export only a single class')
            ])
            ->setDescription('Exports API routes to angular classes')
            ->setHelp(<<<'EOF'
The <info>%command.name%</info> allows to export API routes to angular classes:

  <info>php %command.full_name%</info>

Add <info>--method</info> if you want to render in console a single method from one of detected classes:

  <info>php %command.full_name% --method</info>
  or
  <info>php %command.full_name% -m</info>

Add <info>--class</info> if you want to export only one class:

  <info>php %command.full_name% --class</info>
  or
  <info>php %command.full_name% -c</info>

EOF
            );
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException When route does not exist
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->fileSystem = new Filesystem;
        $this->templating = $templating = $this->getContainer()->get('twig');
        $this->path = $this->getContainer()->getParameter('tofusteak_angular_api.destination_path').'/';

        // Convert ml-serialization to classic symfony serialization.yml
        $this->converter->convert();

        /**
         * Generate :
         * - Model
         * - Services
         * - Routes
         * - Components
         * - Filters
         * - Module
         */
        /** @var ClassMetadata $metadata */
        foreach ($this->reader->getMetadata() as $metadata) {
            $this->entitiesNameList[] = $metadata->getCaretCase();
        }

        $this->generateModels();
        $this->generateServices();
        $this->generateEnums();
        $this->generateSecuredFieldsClass();
//        $this->generateComponents();
//        $this->generateRoutes();

    }

    private function generateModels()
    {
        /** @var ClassMetadata $metadata */
        foreach ($this->reader->metadata as $classMetadata) {
            if (!$this->fileSystem->exists($this->path . '/models/' . $classMetadata->getCaretCase() . '.ts')) {
                $this->fileSystem->dumpFile(
                    $this->path . '/models/' . $classMetadata->getCaretCase() . '.ts',
                    $this->templating->render('@maggle/Model/model.ts.twig', [
                        'metadata' => $this->reader->metadata,
                        'classMetadata' => $classMetadata,
                        'entitiesNameList' => $this->entitiesNameList,
                        'groupsByClass' => $this->reader->groupsByClass,
                    ])
                );
            }

            // Generate index.ts
            $this->fileSystem->dumpFile(
                $this->path . '/models/index.ts',
                $this->templating->render('@maggle/Model/index.ts.twig', ['metadata' => $this->reader->metadata])
            );
        }


        /** @var ClassMetadata $metadata */
        foreach ($this->reader->metadata as $classMetadata) {
            $noVotersGroups = [];
            if (isset($this->reader->groupsByClass[$classMetadata->getCaretCase()])) {
                foreach ($this->reader->groupsByClass[$classMetadata->getCaretCase()] as $group => $fields) {
                    $simpleGroup = preg_replace('/__SECURED_.*/', '', $group);
                    $noVotersGroups[$simpleGroup] = array_merge((isset($noVotersGroups[$simpleGroup]) ? $noVotersGroups[$simpleGroup] : []), $fields);
                }
            } else {
                $noVotersGroups = [];
            }

            $this->fileSystem->dumpFile(
                $this->path.'/base-models/'.$classMetadata->getCaretCase().'.ts',
                $this->templating->render('@maggle/BaseModel/base_model.ts.twig', [
                    'metadata' => $this->reader->metadata,
                    'classMetadata' => $classMetadata,
                    'entitiesNameList' => $this->entitiesNameList,
                    'noVotersGroups' => $noVotersGroups,
                ])
            );
        }

        // Generate index.ts
        $this->fileSystem->dumpFile(
            $this->path.'/base-models/index.ts',
            $this->templating->render('@maggle/BaseModel/index.ts.twig', ['metadata' => $this->reader->metadata])
        );
    }

    private function generateServices()
    {

        /** @var ClassMetadata $metadata */
        foreach ($this->reader->metadata as $classMetadata) {
            if (!$this->fileSystem->exists($this->path . '/services/' . $classMetadata->getCaretCase() . '.ts')) {
                $this->fileSystem->dumpFile(
                    $this->path . '/services/' . $classMetadata->getCaretCase() . '.ts',
                    $this->templating->render('@maggle/Service/service.ts.twig', [
                        'metadata' => $this->reader->metadata,
                        'classMetadata' => $classMetadata,
                        'entitiesNameList' => $this->entitiesNameList,
                        'groupsByClass' => $this->reader->groupsByClass,
                    ])
                );
            }
        }

        // Generate index.ts
        $this->fileSystem->dumpFile(
            $this->path.'/services/index.ts',
            $this->templating->render('@maggle/Service/index.ts.twig', [
                'entitiesNameList' => $this->entitiesNameList,
                'metadata' => $this->reader->metadata,
            ])
        );

        /** @var ClassMetadata $metadata */
        foreach ($this->reader->metadata as $classMetadata) {
            $this->fileSystem->dumpFile(
                $this->path.'/base-services/'.$classMetadata->getCaretCase().'.ts',
                $this->templating->render('@maggle/BaseService/base_service.ts.twig', [
                    'metadata' => $this->reader->metadata,
                    'classMetadata' => $classMetadata,
                    'entitiesNameList' => $this->entitiesNameList,
                    'groupsByClass' => $this->reader->groupsByClass,
                ])
            );
        }

        // Generate index.ts
        $this->fileSystem->dumpFile(
            $this->path.'/base-services/index.ts',
            $this->templating->render('@maggle/BaseService/index.ts.twig', ['metadata' => $this->reader->metadata])
        );
    }

    private function generateEnums()
    {
        /** @var EnumMetadata $enumMetadata */
        foreach ($this->reader->enumMetadata as $enumMetadata) {
            $enumClassName = $enumMetadata->getName();
            $this->fileSystem->dumpFile(
                $this->path.'/enums/'.$enumMetadata->getCaretCase().'.ts',
                $this->templating->render('@maggle/Enum/enum.ts.twig', [
                    'enumMetadata' => $enumMetadata,
                    'values' => $enumClassName::values()
                ])
            );
        }

        // Generate index.ts
        $this->fileSystem->dumpFile(
            $this->path.'/enums/index.ts',
            $this->templating->render('@maggle/Enum/index.ts.twig', [
                'enumMetadata' => $this->reader->enumMetadata,
            ])
        );
    }

    private function generateSecuredFieldsClass()
    {
        // clear model path
        $filePath = $this->getContainer()->getParameter('kernel.root_dir').'/../src/Security/ConditionalFields.php';
        $this->fileSystem->remove($filePath);

        $voters = [];

        /** @var ClassMetadata $metadata */
        foreach ($this->reader->metadata as $classMetadata) {
            if (isset($this->reader->groupsByClass[$classMetadata->getCaretCase()])) {
                foreach ($this->reader->groupsByClass[$classMetadata->getCaretCase()] as $group => $fields) {
                    if (preg_match('/__SECURED_.*/', $group)) {
                        preg_match_all('/(.*)__SECURED_(.*)/', $group, $matches);
                        $voters[$matches[1][0]] = array_merge((isset($voters[$matches[1][0]]) ? $voters[$matches[1][0]] : []), [$matches[2][0] => $matches[2][0]]);
                    }
                }
            }
        }

        $this->fileSystem->dumpFile(
            $filePath,
            $this->templating->render('@maggle/Security/ConditionalFields.php.twig', [
                'voters' => $voters,
                'groups' => preg_replace('/(=>\ )\'(.*)\',/', '$1static::$2,', var_export($voters, true))
            ])
        );
    }


    private function getAttributeMetadata(&$metadata, ResourceMetadata $resource, $attribute, $isProperty = true, $attributeName = null)
    {
        $attributeName = $attributeName ?? $attribute->getName();

        $metadata['properties'][$attributeName] = [
            'type'         => 'string',
            'enumType'     => null,
            'isCollection' => null,
            'validators'   => [],
            'isModel'      => null,
            'nullable'     => true,
            'iri'          => null,
            'isId'         => false,
            'groups'       => null
        ];

        if ($attributeName === 'translations') {
            $metadata['properties'][$attributeName]['type'] = $resource->getShortName().'Translation';
            $metadata['properties'][$attributeName]['isCollection'] = true;
        }

        if (isset($this->yamlSerializationGroups[$resource->getShortName()][$attributeName])) {
            $groups = $this->yamlSerializationGroups[$resource->getShortName()][$attributeName];
            $metadata['properties'][$attributeName]['groups'] = $groups;
            foreach ($groups as $group) {
                $this->generatePropertyGroup($metadata, $resource->getShortName(), $attributeName, $group);
            }
        }

        if ($isProperty) {
            $annotations = $this->annotationReader->getPropertyAnnotations($attribute);
        } else {
            $annotations = $this->annotationReader->getMethodAnnotations($attribute);
        }

        foreach ($annotations as $annotation) {
            switch (true) {
                case $annotation instanceof Column:
                    $metadata['properties'][$attributeName]['nullable'] = $annotation->nullable;
                    switch ($annotation->type) {
                        case 'datetime':
                        case 'phone_number':
                        case 'text':
                        case 'date':
                            $metadata['properties'][$attributeName]['type'] = 'string';
                            break;
                        case 'integer':
                        case 'smallint':
                        case 'float':
                        case 'int':
                            if ($attributeName !== 'id') {
                                $metadata['properties'][$attributeName]['type'] = 'number';
                            }
                            break;
                        default:
                            $metadata['properties'][$attributeName]['type'] = $annotation->type;
                    }
                    break;
                case $annotation instanceof ApiProperty:
                    $metadata['properties'][$attributeName]['iri'] = $annotation->iri;
                    break;
                case $annotation instanceof OneToOne:
                    $metadata['properties'][$attributeName]['type'] = $this->getShortName($annotation->targetEntity);
                    break;
                case $annotation instanceof ManyToOne:
                    $metadata['properties'][$attributeName]['type'] = $this->getShortName($annotation->targetEntity);
                    break;
                case $annotation instanceof OneToMany:
                    $metadata['properties'][$attributeName]['isCollection'] = true;
                    $metadata['properties'][$attributeName]['type'] = $this->getShortName($annotation->targetEntity);
                    break;
                case $annotation instanceof ManyToMany:
                    $metadata['properties'][$attributeName]['isCollection'] = true;
                    $metadata['properties'][$attributeName]['type'] = $this->getShortName($annotation->targetEntity);
                    break;
                case $annotation instanceof Id:
                    $metadata['properties'][$attributeName]['isId'] = true;
                    $metadata['properties'][$attributeName]['isNullable'] = false;
                    break;
                case $annotation instanceof Model:
                    preg_match_all('/.*\\\(.*)$/', $annotation->value, $className);
                    $metadata['properties'][$attributeName]['type'] = $className[1][0];
                    $metadata['properties'][$attributeName]['isModel'] = true;
                    $this->modelClasses[$className[1][0]] = $annotation->value;
                    break;
                case $annotation instanceof Enum:
                    preg_match_all('/.*\\\(.*)$/', $annotation->value, $className);
                    $metadata['properties'][$attributeName]['enumType'] = $className[1][0];
                    $this->enumClasses[$className[1][0]] = $annotation->value;
                    break;
//                    case $methodAnnotation instanceof Groups:
//                         $metadata['properties'][$attribute->getName()]['groups'] = $methodAnnotation->getGroups();
//                        foreach ($methodAnnotation->getGroups() as $group) {
//                            $this->generateAttributeGroup($metadata, $resource->getShortName(), $attribute->getName(), $group);
//                        }
//                        break;
            }
        }
    }

    /**
     * Returns class name from full namespace
     *
     * @param string $namespace
     *
     * @return string
     */
    private function getShortName(string $namespace)
    {

        return preg_replace('/.*\\\\(.*)/', '$1', $namespace);
    }

    private function getCaretCase(string $className)
    {

        return str_replace('_', '-', $this->snakeConverter->normalize(lcfirst($className)));
    }

    function generatePropertyGroup(&$metadata, $resourceName, $propertyName, $group)
    {
        $groupParts = explode('.', $group);
        if (count($groupParts) === 4) {
            list($entity, $role, $action, $alternative) = $groupParts;
        }
        if (count($groupParts) === 3) {
            list($entity, $role, $action) = $groupParts;
            $alternative = null;

            // Ignore unexpected actions
            if (!in_array($action, ['list', 'show', 'edit', 'create'])) {
                return;
            }
        }
        // Ignore < 3 parts groups
        if (count($groupParts) < 3) {
            return;
        }

        if (isset($entity, $role, $action)) {
            if (isset($alternative)) {
                // Add group to groups tree
                if (!isset($this->groups[$role][$entity][$action][$alternative])) {
                    $this->groups = array_merge_recursive(
                        $this->groups,
                        [$role => [$entity => [$action => ['_alternatives' => [$alternative => $group]]]]]
                    );
                }
            } else {
                // Add group to groups tree
                if (!isset($this->groups[$role][$entity][$action])) {
                    $this->groups = array_merge_recursive(
                        $this->groups,
                        [$role => [$entity => [$action => $group]]]
                    );
                }
            }
        }

        if (!isset($metadata['class']['groups'][$group])) {
            $metadata['class']['groups'][$group] = [$propertyName];
        } else {
            $metadata['class']['groups'][$group][] = $propertyName;
        }

        if (!isset($this->propertiesByGroup[$group])) {
            $this->propertiesByGroup[$group] = [$resourceName => [$propertyName]];
        } elseif (!isset($this->propertiesByGroup[$group][$resourceName])) {
            $this->propertiesByGroup[$group][$resourceName] = [$propertyName];
        } elseif (!in_array($propertyName, $this->propertiesByGroup[$group][$resourceName])) {
            $this->propertiesByGroup[$group][$resourceName][] = $propertyName;
        }
    }
}
