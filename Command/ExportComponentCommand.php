<?php

namespace Tofusteak\AngularApiBundle\Command;

use Dunglas\ApiBundle\Api\Operation\Operation;
use Dunglas\ApiBundle\Api\Resource;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Route;

/**
 * A console command to export routes to angular2 classes
 */
class ExportComponentCommand extends ContainerAwareCommand
{
    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return parent::isEnabled();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('angular:export:component')
            ->setDefinition([
                new InputOption('method', 'm', InputOption::VALUE_NONE, 'Export only a single method from a class'),
                new InputOption('class', 'c', InputOption::VALUE_NONE, 'Export only a single class')
            ])
            ->setDescription('Exports API routes to angular classes')
            ->setHelp(<<<'EOF'
The <info>%command.name%</info> allows to export API routes to angular classes:

  <info>php %command.full_name%</info>

Add <info>--method</info> if you want to render in console a single method from one of detected classes:

  <info>php %command.full_name% --method</info>
  or
  <info>php %command.full_name% -m</info>

Add <info>--class</info> if you want to export only one class:

  <info>php %command.full_name% --class</info>
  or
  <info>php %command.full_name% -c</info>

EOF
            )
        ;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException When route does not exist
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io                 = new SymfonyStyle($input, $output);
        $this->fileSystem   = new Filesystem;
        $path = $this->getContainer()->getParameter('tofusteak_angular_api.destination_path');
        $resourceCollection = $this->getContainer()->get('api.resource_collection');

//        dump($resourceCollection->getResourceForShortName('File'));

        $routes = $this->getContainer()->get('router')->getRouteCollection();

        $resources = [];
        foreach ($routes as $route) {
            $resourceName = $route->getDefault('_resource');
            $resource = $resourceCollection->getResourceForShortName($resourceName);
            if ($resource) {
                $resources[$resourceName] = $resource;
            }
        }

        foreach ($resources as $resourceName => $resource) {
            $resourcePath = sprintf('%s/%s/', $path, strtolower($resourceName));
            $this->fileSystem->mkdir($resourcePath);

            // For each resource, search for basic cgetAction, getAction, putAction, cpostAction
            // And create associated show, edit and create
            /** @var Resource $resource */
            foreach ($resource->getItemOperations() as $operation) {
                $name = preg_replace('/.*\:\:(.*)Action$/', '$1', $operation->getRoute()->getDefault('_controller'));
                switch($name) {
                    case 'get':
                        $this->createShowClass($operation, $resourceName, $resource, $resourcePath);
                }

            }
        }
    }

    /**
     * Writes angular2 class representing a class to file
     *
     * @param Operation $operation
     * @param string    $resourceName
     * @param Resource  $resource
     * @param string    $path
     *
     * @throws \Twig_Error
     */
    private function createShowClass(Operation $operation, $resourceName, Resource $resource, $resourcePath) {
        $this->fileSystem->mkdir($resourcePath.'show/');

        $annotationReader = $this->getContainer()->get('annotation_reader');
        $class = new \ReflectionClass($resource->getEntityClass());
        $classAnnotations = $annotationReader->getClassAnnotations($class);

        $properties = $class->getProperties();
        foreach ($properties as $property) {
//            dump($property);
            $methodAnnotations = $annotationReader->getPropertyAnnotations($property);
//            dump($methodAnnotations);
        }

        $this->fileSystem->dumpFile(
            $path.'/show/show.ts',
            $this->getContainer()->get('templating')->render('TofusteakAngularApiBundle:Component/Show:show.ts.twig', ['resource' => $resource, 'resourceName' => $resourceName])
        );

        $this->fileSystem->dumpFile(
            $path.'/show/show.html',
            $this->getContainer()->get('templating')->render('TofusteakAngularApiBundle:Component/Show:show.html.twig', ['resource' => $resource, 'resourceName' => $resourceName])
        );
    }
}
