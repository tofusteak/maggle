<?php

namespace Tofusteak\AngularApiBundle;


final class MaggleEvents
{
    /**
     * When user requests a new password
     */
    const PASSWORD_REQUESTED = 'app.password_requested';

    /**
     * When user requests a new password
     */
    const PASSWORD_RESET = 'app.password_reset';
}