<?php

namespace Tofusteak\AngularApiBundle\Security\User;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserProvider implements UserProviderInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository($this->container->getParameter('tofusteak_angular_api.user_entity'))
            ->findOneByEmail($username);

        if (!$user) {
            $message = sprintf(
                'Unable to find an active user identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->container->get('doctrine.orm.entity_manager')
            ->getRepository($this->container->getParameter('user'))
            ->find($user->getId());
    }

    public function supportsClass($class)
    {
        return true;
//        return $this->userRepository->getClassName() === $class
//        || is_subclass_of($class, $this->userRepository->getClassName());
    }
}
