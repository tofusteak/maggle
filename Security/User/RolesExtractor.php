<?php

namespace Tofusteak\AngularApiBundle\Security\User;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class RolesExtractor
{
    protected $tokenStorage;
    protected $roleHierarchy;

    public function __construct(RoleHierarchyInterface $roleHierarchy, TokenStorageInterface $tokenStorage)
    {
        $this->roleHierarchy = $roleHierarchy;
        $this->tokenStorage  = $tokenStorage;
    }

    /**
     * Get all role labels for connected user
     *
     * @return array
     */
    public function getRoleLabels() : array
    {
        return array_map(function ($role) {
            // @todo uncomment and remove the other I put the other to be compatible with Jeremy's conditionnal field system
            // @todo but IMO we should not replace _ by -;
//            return strtolower(preg_replace('/^ROLE_/', '', $role));
            return strtolower(str_replace('_', '-', preg_replace('/^ROLE_/', '', $role)));
        }, $this->getRoles());


    }

    /**
     * Get all roles for connected user
     *
     * @return array
     */
    public function getRoles() : array
    {
        $tokenRoles = $this->tokenStorage->getToken()->getRoles();
        $reachableRoles = $this->roleHierarchy->getReachableRoles($tokenRoles);

        return array_unique(
            array_map(
                function (Role $role) { return $role->getRole(); },
                $reachableRoles
            )
        );
    }
}
