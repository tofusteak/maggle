<?php

namespace Tofusteak\AngularApiBundle\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Service("app.security.voter.base_voter", public=false, abstract=true)
 */
abstract class BaseVoter implements VoterInterface
{
    /**
     * @var RoleHierarchyInterface
     */
    protected $roleHierarchy;

    // These two should only be accessed in vote() or doVote()

    /**
     * @var TokenInterface|null
     */
    protected $currentToken;

    /**
     * @var mixed
     */
    protected $currentObject;

    /**
     * Check if the voted class is supported by this voter
     *
     * @param string|null $class
     * @return boolean
     */
    abstract public function supportsClass($class);

    /**
     * @param $roleHierarchy
     *
     */
    public function setRoleHierarchy($roleHierarchy)
    {
        $this->roleHierarchy = new RoleHierarchy($roleHierarchy);
    }

    /**
     * @return array
     */
    abstract protected function getSupportedAttributes();

    /**
     * @param mixed $attribute
     * @return boolean
     */
    public function supportsAttribute($attribute)
    {
        return in_array($attribute, $this->getSupportedAttributes());
    }

    /**
     * {@inheritDoc}
     */
    public function vote(TokenInterface $token, $object, array $attributes)
    {
        // check if class of this object is supported by this voter
        if (!$this->supportsClass(null === $object ? null : get_class($object))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // check if the given attribute(s) is covered by this voter
        if (1 !== count($attributes)) {
            throw new \InvalidArgumentException(
                'Only one attribute at a time is allowed for this Voter.'
            );
        }

        $attribute = reset($attributes);
        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        if (null === $this->currentToken) {
            $this->currentToken  = $token;
            $this->currentObject = $object;
        }

        try {
            return $this->doVote($attribute);
        } finally {
            $this->currentToken  = null;
            $this->currentObject = null;
        }
    }

    /**
     * Reentrant method to actually implement
     * the several steps of the voting process.
     *
     * @param string $attribute
     * @return int either ACCESS_GRANTED, ACCESS_ABSTAIN, or ACCESS_DENIED
     */
    protected function doVote($attribute)
    {
        if (VoterInterface::ACCESS_ABSTAIN !== ($vote = $this->voteOnObject($attribute, $this->currentObject))) {
            return $vote;
        }

        $roles = $this->currentToken->getRoles();
        $user =  $this->currentToken->getUser();

        // make sure there is a user object (i.e. that the user is logged in and recognized by the system)
        if ($user instanceof UserInterface) {
            if (VoterInterface::ACCESS_ABSTAIN !== ($vote = $this->voteOnUser($attribute, $this->currentObject, $user))) {
                return $vote;
            }
        }
        $roles = array_unique($this->roleHierarchy->getReachableRoles($roles), SORT_REGULAR);

        foreach ($roles as $role) {
            if (is_string($role)) {
                $role = new Role($role);
            }
            if (!($role instanceof RoleInterface)) {
                continue;
            }
            if (VoterInterface::ACCESS_ABSTAIN !== ($vote = $this->voteOnRole($attribute, $this->currentObject, $role, $user))) {
                return $vote;
            }
        }

        if ($this->voteOnAttribute($attribute, $this->currentObject, $this->currentToken)) {
            // grant access as soon as at least one attribute returns a positive response
            return self::ACCESS_GRANTED;
        }

//        if (!($user instanceof UserInterface) && 0 === count($roles)) {
//            if (VoterInterface::ACCESS_ABSTAIN !== ($vote = $this->voteOnAnonymousUser($attribute, $this->currentObject, $user))) {
//                return $vote;
//            }
//        }

        return $this->defaultVote($attribute);
    }

    /**
     * Carry on voting purely on the Object.
     *
     * Override this function to grant permissions to everybody.
     *
     * @param string $attribute
     * @param mixed $object
     * @return int either ACCESS_GRANTED, ACCESS_ABSTAIN, or ACCESS_DENIED
     */
    protected function voteOnObject($attribute, $object = null)
    {
        return VoterInterface::ACCESS_ABSTAIN;
    }

    /**
     * Carry on voting on the User.
     *
     * Override this function to grant access to specific users.
     *
     * @param UserInterface $user Grantee (who do you grant to)
     * @param string $attribute Granted (which permission do you grant)
     * @param object $object Context (what do you grant for)
     * @return int either ACCESS_GRANTED, ACCESS_ABSTAIN, or ACCESS_DENIED
     */
    protected function voteOnUser($attribute, $object, UserInterface $user)
    {
        return VoterInterface::ACCESS_ABSTAIN;
    }

    /**
     * Carry on voting on the Roles.
     *
     * Override this function to grant permissions based on roles.
     *
     * Default policy is to let SUPER_ADMINs to do anything.
     *
     * @param string $attribute
     * @param object $object
     * @param RoleInterface $role
     * @param mixed $user
     * @return int either ACCESS_GRANTED, ACCESS_ABSTAIN, or ACCESS_DENIED
     */
    protected function voteOnRole($attribute, $object, RoleInterface $role, $user)
    {
        return ('ROLE_SUPER_ADMIN' === $role->getRole())
            ? VoterInterface::ACCESS_GRANTED
            : VoterInterface::ACCESS_ABSTAIN;
    }

    /**
     * Carry on voting in case of Anonymous user.
     *
     * Called if the user is not a UserInterface and no ROLE are available.
     *
     * Override this function to grant permissions to annonymous users.
     *
     * @param string $attribute
     * @param mixed $object
     * @param mixed $user
     * @return int either ACCESS_GRANTED, ACCESS_ABSTAIN, or ACCESS_DENIED
     */
    protected function voteOnAnonymous($attribute, $object, $user)
    {
        return VoterInterface::ACCESS_ABSTAIN;
    }

    /**
     * Provides default decision for each attribute.
     *
     * Override this function to grant permissions to "anybody else".
     *
     * DO NOT return VoterInterface::ACCESS_ABSTAIN unless you know what you're
     * doing.
     *
     * Default policy is to forbid access by default.
     *
     * @param string $attribute
     * @return int either ACCESS_GRANTED, ACCESS_ABSTAIN, or ACCESS_DENIED
     */
    protected function defaultVote($attribute)
    {
        return VoterInterface::ACCESS_DENIED;
    }

    /**
     * Reentrant method to easily check if another attribute is granted.
     *
     * @param mixed $attribute
     * @param boolean $default
     * @return boolean
     */
    protected function isGranted($attribute, $default = false)
    {
        $vote = $this->doVote($attribute);

        return VoterInterface::ACCESS_ABSTAIN !== $vote ? VoterInterface::ACCESS_GRANTED === $vote : $default;
    }
}
