<?php

namespace Tofusteak\AngularApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tofusteak_angular_api');

        $rootNode
            ->children()
                ->scalarNode('destination_path')->defaultValue('%kernel.project_dir%/../client/src/shared')->end()
                ->scalarNode('tmp_upload_dir')->defaultValue('%kernel.project_dir%/public/uploads')->end()
                ->scalarNode('upload_url')->defaultValue('/uploads/')->end()
                ->scalarNode('user_entity')->defaultValue('App\Entity\User')->end()
            ->end()
        ;

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
