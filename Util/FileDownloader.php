<?php

namespace Tofusteak\AngularApiBundle\Util;

class FileDownloader
{
    public static function loadImage($url, $dest)
    {
        $errors = [];
        try {
            $prev = \set_error_handler(function ($level, $message, $file) use (&$errors) {
                if (__FILE__ === $file) {
                    $errors[] = ['level' => $level, 'message' => $message];
                }
            });
            $bin = new \SplFileInfo($dest);

            mkdir(pathinfo($dest)['dirname']);
            if (false === $bin || !($content = @\file_get_contents($url))) {
                throw new ImageDownloadException('Impossible to download ' . $url . ' ('.\json_encode($errors).')');
            }
        } finally {
            if ($prev) {
                \set_error_handler($prev);
            }
        }

        \file_put_contents($bin->getPathname(), $content);
        return $bin;
    }

}
