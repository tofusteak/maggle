<?php

namespace Tofusteak\AngularApiBundle\Util;


use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\RequestOptions;

class CollectWalletUtil
{
    protected $requestStack;
    protected $login;
    protected $pass;
    protected $version;
    protected $walletUa;
    protected $sandbox;
    protected $client;

    public function __construct(RequestStack $requestStack, string $login, string $pass, string $version, string $walletUa, bool $sandbox)
    {
        $this->requestStack = $requestStack;
        $this->login = $login;
        $this->pass = $pass;
        $this->version = $version;
        $this->walletUa = $walletUa;

        $request = $this->requestStack->getCurrentRequest();

        $this->walletUa = $request ? $request->headers->get('User-Agent') : 'Postman';
        $this->sandbox = $sandbox;

        if ($sandbox) {
            $apiUrl = 'https://sandbox-api.lemonway.fr/mb/lwcollect/dev/collect_json/service_json.asmx/';
        } else {
            // @todo find the live url because it's not live
            $apiUrl = 'https://api.lemonway.fr/directkitjson2/Service.asmx/';
        }

        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $apiUrl,
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
    }

    public function createWallet(string $walletId, $email, $title, $firstname, $lastname, $phoneNumber): ResponseInterface
    {
        $params = ['p' => [
            'wallet' => $walletId,

            'clientMail' => $email,
            'clientFirstName' => $firstname,
            'clientLastName'=> $lastname,
            'phoneNumber' => "33672635263",
            'isCompany' => false,

            "wlLogin" => $this->login,
            "wlPass" => $this->pass,
            "language" => "en",
            "version" => $this->version,
            "walletIp" => $this->getClientIp(),
            "walletUa" => $this->walletUa,
        ]];

        dump($params);
        return $this->client->post('RegisterWallet', [
            RequestOptions::JSON => $params,
            'headers' => [
                "Content-type: application/json;charset=utf-8",
                "Accept: application/json",
                "Cache-Control: no-cache",
                "Pragma: no-cache"
            ],
        ]);
    }

    public function creditWallet(string $walletId)
    {

    }


    protected function getClientIp(): string
    {
        $request = $this->requestStack->getCurrentRequest();
        return $request ? $request->getClientIp() : '1.1.1.1';
    }


}
