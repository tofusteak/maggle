<?php

namespace Tofusteak\AngularApiBundle\Serializer;

use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Security\ConditionalFields;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Role\Role;
use Tofusteak\AngularApiBundle\Serialization\GroupsExtractor;

/**
 * {@inheritdoc}
 */
final class SerializerContextBuilder implements SerializerContextBuilderInterface
{
    /**
     * @var ResourceMetadataFactoryInterface
     */
    private $resourceMetadataFactory;

    /**
     * @var array
     */
    private $rolesHierarchy;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * @var GroupsExtractor
     */
    private $groupsExtractor;

    public function __construct(ResourceMetadataFactoryInterface $resourceMetadataFactory, TokenStorageInterface $tokenStorage, array $rolesHierarchy, AuthorizationCheckerInterface $authChecker, GroupsExtractor $groupsExtractor)
    {
        $this->resourceMetadataFactory = $resourceMetadataFactory;
        $this->rolesHierarchy = $rolesHierarchy;
        $this->tokenStorage = $tokenStorage;
        $this->authChecker = $authChecker;
        $this->groupsExtractor = $groupsExtractor;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromRequest(Request $request, bool $normalization, array $attributes = null) : array
    {
        if (null === $attributes) {
            // @todo search an alternative at RequestAttributesExtractor which is internal (if we rely on it, it can broke on updating ApiPlatform)
            $attributes = RequestAttributesExtractor::extractAttributes($request);
        }

        // Add route params to attributes
        $attributes = array_merge($attributes, $request->attributes->get('_route_params'));

        $context = $this->createFromAttributes($attributes);
        $context['request_uri'] = $request->getRequestUri();

        return $context;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromAttributes(array $attributes = null) : array
    {
        $resourceMetadata = $this->resourceMetadataFactory->create($attributes['resource_class']);
        $resourceName = lcfirst($resourceMetadata->getShortName());
        $action = '';

        if (isset($attributes['_api_serialization_target_action'])) {
            $action = $attributes['_api_serialization_target_action'];
        } elseif (isset($attributes['collection_operation_name'])) {
            switch ($attributes['collection_operation_name']) {
                case 'get':
                    $action = 'list';
                    break;
                case 'special':
                    $action = 'list';
                case 'post':
                    $action = 'edit';
                    break;
                default:
                    $action = $attributes['collection_operation_name'];
            }
            $context['collection_operation_name'] = $attributes['collection_operation_name'];
        } else {
            switch ($attributes['item_operation_name']) {
                case 'get':
                    $action = 'show';
                    break;
                case 'put':
                    $action = 'edit';
                    break;
                default:
                    $action = $attributes['item_operation_name'];
            }
            $context['item_operation_name'] = $attributes['item_operation_name'];
        }

        $context['groups'] = $this->groupsExtractor->getSerializationGroups($resourceName, $action, $attributes['resource_class']);
        $context['resource_class'] = $attributes['resource_class'];

        return $context;
    }
}
