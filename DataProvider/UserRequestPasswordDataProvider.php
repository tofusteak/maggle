<?php

namespace Tofusteak\AngularApiBundle\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\Exception\RuntimeException;
use App\Entity\User;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class UserRequestPasswordDataProvider implements ItemDataProviderInterface, CollectionDataProviderInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManager         $entityManager
     * @param ManagerRegistry       $managerRegistry
     */
    public function __construct(TokenStorageInterface $tokenStorage, ManagerRegistry $managerRegistry)
    {
        $this->tokenStorage = $tokenStorage;
        $this->managerRegistry = $managerRegistry;
    }


    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        if (User::class !== $resourceClass || !filter_var($id, FILTER_VALIDATE_EMAIL)) {
            throw new ResourceClassNotSupportedException();
        }

        $user = $this->managerRegistry->getRepository('App:User')->findOneByEmail($id);
        if ($user) {
            return $user;
        }

        throw new ResourceClassNotSupportedException();
    }


    public function getCollection(string $resourceClass, string $operationName = null, bool $fetchData = false)
    {
        throw new ResourceClassNotSupportedException();
    }
}